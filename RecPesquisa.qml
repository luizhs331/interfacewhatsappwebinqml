import QtQuick 2.2
import QtQuick.Controls 2.0

Rectangle {
    id: recPesquisa
    color: "#f6f6f6"

    property var placeHolderText: "Pesquisar ou começar uma nova conversa"

    PropertyAnimation {
        id: animationColor
        target: recPesquisa
        property: "color"
        duration: 300
    }

    Rectangle {
        color: "#fff"
        width: parent.width / 1.05
        height: parent.height / 1.5
        anchors.centerIn: parent
        radius: 15

        Row {
            id: rowPesquisa
            width: parent.width / 1.1
            height: parent.height
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 20

            Image {
                id: imgLupa
                fillMode: Image.PreserveAspectFit
                source: iconSearch
                anchors.verticalCenter: parent.verticalCenter
            }

            Image {
                id: imageArrow
                source: iconArrow
                rotation: 270
                fillMode: Image.PreserveAspectFit
                anchors.verticalCenter: parent.verticalCenter
                visible: false
            }

            RotationAnimator {
                id: rotation
                target: imageArrow
                from: 270
                to: 360
                duration: 5000
            }

            TextField {
                id: textNome
                width: (parent.width - imgLupa.width) - rowPesquisa.spacing
                height: parent.height
                placeholderText: placeHolderText
                background: Rectangle {
                    border.color: "transparent"
                    color: "transparent"
                }
                onFocusChanged: {
                    imageArrow.visible = imgLupa.visible
                    imgLupa.visible = !imgLupa.visible

                    textNome.placeholderText = imageArrow.visible ? "" : placeHolderText
                    animationColor.to = imageArrow.visible ? "#fff" : "#f6f6f6"

                    animationColor.running = true
                    rotation.running = imageArrow.visible
                }
            }
        }
    }
}
