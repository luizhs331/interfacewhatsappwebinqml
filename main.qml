import QtQuick 2.2
import QtQuick.Window 2.12
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

import "componentes"

Window {
    id: root
    width: 1600
    height: 900
    visible: true
    title: qsTr("Hello World")
    color: "#dddbd1"

    Pane {
        anchors.fill: parent
        focusPolicy: Qt.ClickFocus
    }

    // TODO create a helper from icons
    property var iconStatus: "data:image/svg+xml;utf8,<svg id=\"ee51d023-7db6-4950-baf7-c34874b80976\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\" width=\"24\" height=\"24\"><path fill=\"#919191\" d=\"M12 20.664a9.163 9.163 0 0 1-6.521-2.702.977.977 0 0 1 1.381-1.381 7.269 7.269 0 0 0 10.024.244.977.977 0 0 1 1.313 1.445A9.192 9.192 0 0 1 12 20.664zm7.965-6.112a.977.977 0 0 1-.944-1.229 7.26 7.26 0 0 0-4.8-8.804.977.977 0 0 1 .594-1.86 9.212 9.212 0 0 1 6.092 11.169.976.976 0 0 1-.942.724zm-16.025-.39a.977.977 0 0 1-.953-.769 9.21 9.21 0 0 1 6.626-10.86.975.975 0 1 1 .52 1.882l-.015.004a7.259 7.259 0 0 0-5.223 8.558.978.978 0 0 1-.955 1.185z\"></path></svg>"
    property var iconMensage: "data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\" width=\"24\" height=\"24\"><path fill=\"#919191\" d=\"M19.005 3.175H4.674C3.642 3.175 3 3.789 3 4.821V21.02l3.544-3.514h12.461c1.033 0 2.064-1.06 2.064-2.093V4.821c-.001-1.032-1.032-1.646-2.064-1.646zm-4.989 9.869H7.041V11.1h6.975v1.944zm3-4H7.041V7.1h9.975v1.944z\"></path></svg>"
    property var iconThreePoints: "data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\" width=\"24\" height=\"24\"><path fill=\"#919191\" d=\"M12 7a2 2 0 1 0-.001-4.001A2 2 0 0 0 12 7zm0 2a2 2 0 1 0-.001 3.999A2 2 0 0 0 12 9zm0 6a2 2 0 1 0-.001 3.999A2 2 0 0 0 12 15z\"></path></svg>"
    property var iconArrow: "data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\" width=\"24\" height=\"24\"><path fill=\"#33b7f6\" d=\"M12 4l1.4 1.4L7.8 11H20v2H7.8l5.6 5.6L12 20l-8-8 8-8z\"></path></svg>"
    property var iconSearch: "data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\" width=\"24\" height=\"24\"><path fill=\"#919191\" d=\"M15.9 14.3H15l-.3-.3c1-1.1 1.6-2.7 1.6-4.3 0-3.7-3-6.7-6.7-6.7S3 6 3 9.7s3 6.7 6.7 6.7c1.6 0 3.2-.6 4.3-1.6l.3.3v.8l5.1 5.1 1.5-1.5-5-5.2zm-6.2 0c-2.6 0-4.6-2.1-4.6-4.6s2.1-4.6 4.6-4.6 4.6 2.1 4.6 4.6-2 4.6-4.6 4.6z\"></path></svg>"

    Rectangle {
        width: parent.width
        height: parent.height / 6.5
        color: "#1b9a59"
        anchors.horizontalCenter: parent.horizontalCenter
    }

    //    Rectangle {
    //        color: "#000"
    //        width: parent.width
    //        height: parent.height
    //        opacity: 0.1
    //    }
    RectangularGlow {
        id: effectShadow
        anchors.fill: recCenter
        glowRadius: 1
        cornerRadius: 10
        spread: 0
        color: "#000"
        opacity: 0.5
    }
    Rectangle {
        id: recCenter
        width: root.width / 1.15
        height: root.height / 1.05
        anchors.centerIn: parent
        color: "#fff"

        Rectangle {
            anchors.fill: parent
            Image {
                anchors.fill: parent
                source: "img/background-whatsapp.PNG"
            }

            Rectangle {
                id: recTopFriend
                width: parent.width - recContatos.width
                height: parent.height / 12
                anchors.right: parent.right
                color: "#ededed"

                Row {
                    width: parent.width / 1.13
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter

                    Rectangle {
                        id: recImgFriend
                        width: imgFriend.width
                        height: imgFriend.height
                        color: "transparent"
                        anchors.verticalCenter: parent.verticalCenter

                        Image {
                            id: imgFriend
                            source: "img/landscape-color.jpg"
                            width: 40
                            height: 40
                            fillMode: Image.PreserveAspectCrop
                            layer.enabled: true
                            anchors.verticalCenter: parent.verticalCenter

                            layer.effect: OpacityMask {
                                maskSource: recMaskFriend
                            }
                        }

                        Rectangle {
                            id: recMaskFriend
                            width: imgFriend.width
                            height: imgFriend.height
                            color: "#fff"
                            radius: 360
                            visible: false
                        }
                    }

                    Row {
                        anchors.verticalCenter: parent.verticalCenter
                        width: (parent.width - recImgFriend.width) / 7
                        anchors.right: parent.right
                        spacing: 20

                        IconWithAnimation {
                            iconUrl: iconSearch
                        }

                        IconWithAnimation {
                            iconUrl: iconThreePoints
                        }
                    }
                }
            }
        }

        Row {
            anchors.centerIn: parent
            width: parent.width
            height: parent.height

            Rectangle {
                id: recContatos
                width: parent.width / 3.2
                height: parent.height
                color: "#fff"

                Column {
                    width: parent.width
                    height: parent.height

                    BarProfile {
                        id: recTop
                        width: parent.width
                        height: parent.height / 12
                    }

                    RecPesquisa {
                        id: recPesquisa
                        width: parent.width
                        height: parent.height / 14
                    }

                    Rectangle {
                        width: parent.width
                        height: 1
                        color: "#f2f2f2"
                    }

                    Flickable {
                        id: flickable
                        width: parent.width
                        height: (parent.height - recPesquisa.height) - recTop.height
                        contentHeight: parent.height
                        clip: true

                        Column {
                            width: parent.width

                            Repeater {
                                model: 10
                                RecContato {
                                    width: parent.width
                                    height: recCenter.height / 10
                                }
                            }
                        }

                        // TODO arrumar o tamanho da barra
                        ScrollBar.vertical: ScrollBar {
                            policy: ScrollBar.AlwaysOn
                            hoverEnabled: true
                        }
                    }
                }
            }
            Rectangle {
                width: 1
                height: parent.height
                color: "#e5e5e6"
            }

            CenterDefault {
                width: parent.width - recContatos.width
                height: parent.height
                visible: false
            }
        }
    }
}
