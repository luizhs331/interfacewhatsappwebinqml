import QtQuick 2.0
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

Rectangle {
    id: root
    color: mouseArea.containsMouse ? "#ebebeb" : "#fff"

    Row {
        id: rowContatos
        anchors.fill: parent
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: 20
        spacing: 20

        Rectangle {
            width: imgContato.width
            height: imgContato.height
            color: "transparent"
            anchors.verticalCenter: parent.verticalCenter

            Image {
                id: imgContato
                source: "img/unknown_person.jpg"
                width: 50
                height: 50
                fillMode: Image.PreserveAspectCrop
                layer.enabled: true
                anchors.verticalCenter: parent.verticalCente

                layer.effect: OpacityMask {
                    maskSource: recMaskContato
                }
            }

            Rectangle {
                id: recMaskContato
                width: imgContato.width
                height: imgContato.height
                color: "#fff"
                radius: 360
                visible: false
            }
        }

        Rectangle {
            width: (parent.width - rowContatos.spacing) - imgContato.width
            height: parent.height
            color: root.color

            Column {
                width: parent.width
                height: parent.height / 2
                anchors.verticalCenter: parent.verticalCenter

                Row {
                    width: parent.width
                    height: parent.height / 2

                    Label {
                        text: "Bonde da pinadeira"
                        font.pointSize: 13
                        font.family: "arial"
                        color: "#000"
                    }

                    Label {
                        text: "Ontem"
                        font.pointSize: 11
                        font.family: "Helvetica"
                        color: "#919191"
                        anchors.right: parent.right
                        anchors.rightMargin: 30
                    }
                }
                Label {
                    text: "Joelma: Bora cs ?"
                    font.pointSize: 11
                    font.family: "Helvetica"
                    color: "#919191"
                }
            }
            Rectangle {
                width: parent.width
                height: 1
                color: "#f2f2f2"
                anchors.bottom: parent.bottom
            }
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
    }
}
