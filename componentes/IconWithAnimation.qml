import QtQuick 2.2
import QtQuick.Controls 2.0

Rectangle {
    id: root
    width: 38
    height: 38
    color: mouseAreaRoot.pressed ? "#d5d5d5" : "transparent"
    radius: 360

    property var iconUrl: ""

    Image {
        anchors.centerIn: parent
        source: iconUrl
    }

    MouseArea {
        id: mouseAreaRoot
        anchors.fill: parent
        hoverEnabled: true
    }
}
