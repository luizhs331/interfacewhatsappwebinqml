import QtQuick 2.2
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

Rectangle {
    id: recTop
    color: "#ededed"

    Row {
        width: parent.width / 1.13
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        Rectangle {
            id: recImgUser
            width: img.width
            height: img.height
            color: "transparent"
            anchors.verticalCenter: parent.verticalCenter

            Image {
                id: img
                source: "../img/landscape-color.jpg"
                width: 40
                height: 40
                fillMode: Image.PreserveAspectCrop
                layer.enabled: true
                anchors.verticalCenter: parent.verticalCente

                layer.effect: OpacityMask {
                    maskSource: recMask
                }
            }

            Rectangle {
                id: recMask
                width: img.width
                height: img.height
                color: "#fff"
                radius: 360
                visible: false
            }
        }

        Row {
            anchors.verticalCenter: parent.verticalCenter
            width: (parent.width - recImgUser.width) / 2.4
            anchors.right: parent.right
            spacing: 20

            IconWithAnimation {
                iconUrl: iconStatus
            }

            IconWithAnimation {
                iconUrl: iconMensage
            }

            IconWithAnimation {
                iconUrl: iconThreePoints
            }
        }
    }
}
