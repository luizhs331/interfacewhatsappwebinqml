import QtQuick 2.0
import QtQuick.Controls 2.0

Rectangle {
    id: root
    color: "#f8f9fa"

    Column {
        spacing: 20
        anchors.centerIn: parent
        height: parent.height / 1.2

        Image {
            source: "img/whatsapp-intro-connection.jpg"
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Label {
            text: "Mantenha seu celular conectado"
            font.pixelSize: 35
            font.family: "Helvetica"
            anchors.horizontalCenter: parent.horizontalCenter
            color: "#5f6060"
        }

        Label {
            text: "O WhatsApp conecta ao seu celular para sincronizar suas mensagens. Para reduzir o uso de dados, conecte seu celular a uma rede Wi-Fi."
            font.family: "Helvetica"
            font.pointSize: 10
            anchors.horizontalCenter: parent.horizontalCenter
            wrapMode: Text.Wrap
            width: parent.width
            color: "#919191"
            //                        textFormat: Text.StyledText
            horizontalAlignment: Text.AlignHCenter
        }

        Rectangle {
            width: parent.width
            height: 1
            color: "#e5e5e6"
        }

        Rectangle {
            width: parent.width
            height: parent.height
            color: "transparent"
            anchors.horizontalCenter: parent.horizontalCenter

            Row {
                spacing: 5
                anchors.horizontalCenter: parent.horizontalCenter

                Image {
                    source: "img/laptop.png"
                }

                Label {
                    text: "O WhatsApp está disponível para Windows."
                    font.pointSize: 10
                    color: "#919191"
                }
                Label {
                    text: "Baixe aqui."
                    color: "#3eca74"
                    font.pointSize: 10
                }
            }
        }
    }

    Rectangle {
        width: parent.width
        height: 5
        color: "#4adf83"
        anchors.bottom: parent.bottom
    }
}
